package link.jfire.baseutil.tx;

public interface AutoCloseManager
{
    /**
     * 自动关闭对应的资源
     */
    public void close();
}
