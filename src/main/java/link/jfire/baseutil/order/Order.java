package link.jfire.baseutil.order;

public interface Order
{
    /**
     * 返回顺序
     * 
     * @return
     */
    public int getOrder();
}
