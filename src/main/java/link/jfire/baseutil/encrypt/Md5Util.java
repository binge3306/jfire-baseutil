package link.jfire.baseutil.encrypt;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import link.jfire.baseutil.StringUtil;

public class Md5Util
{
    private static Charset charset = Charset.forName("UTF-8");
    
    public static byte[] md5(byte[] array)
    {
        try
        {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] result = md.digest(array);
            return result;
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e);
        }
    }
    
    public static byte[] md5(String str)
    {
        try
        {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] data = str.getBytes(charset);
            byte[] result = md.digest(data);
            return result;
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e);
        }
    }
    
    public static String md5Str(String str)
    {
        return StringUtil.toHexString(md5(str));
    }
    
    public static void main(String[] args)
    {
        System.out.println(Md5Util.md5Str("wodexiaojing"));
    }
}
