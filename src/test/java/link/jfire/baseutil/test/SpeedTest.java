package link.jfire.baseutil.test;

import link.jfire.baseutil.Timelog;
import link.jfire.baseutil.collection.StringCache;
import link.jfire.baseutil.simplelog.ConsoleLogFactory;
import link.jfire.baseutil.simplelog.Logger;
import org.junit.Test;

public class SpeedTest
{
    private Logger logger = ConsoleLogFactory.getLogger(ConsoleLogFactory.DEBUG);
    
    @Test
    public void StringBuilderAndStringCache()
    {
        int count = 1000000;
        Timelog timelog = new Timelog();
        StringBuilder builder = new StringBuilder(2 * count);
        StringCache cache = new StringCache(2 * count);
        timelog.start();
        for (int i = 0; i < count; i++)
        {
            cache.append("你好");
        }
        cache.toString();
        timelog.end();
        logger.debug("cache使用的时间是{}", timelog.total());
        timelog.start();
        for (int i = 0; i < count; i++)
        {
            builder.append("你好");
        }
        cache.toString();
        timelog.end();
        logger.debug("builder使用的时间是{}", timelog.total());
    }
}
