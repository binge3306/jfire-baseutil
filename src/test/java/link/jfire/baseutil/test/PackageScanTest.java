package link.jfire.baseutil.test;

import link.jfire.baseutil.PackageScan;
import org.junit.Assert;
import org.junit.Test;

public class PackageScanTest
{
    @Test
    public void test()
    {
        String[] classNames = PackageScan.scan("link.jfire:out~*collection");
        for (String each : classNames)
        {
            System.out.println(each);
            Assert.assertFalse(each.startsWith("link.jfire.baseutil.collection"));
        }
        System.out.println("***************");
        classNames = PackageScan.scan("link.jfire:in~*collection");
        for (String each : classNames)
        {
            System.out.println(each);
            Assert.assertTrue(each.startsWith("link.jfire.baseutil.collection"));
        }
    }
}
